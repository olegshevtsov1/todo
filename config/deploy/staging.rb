server 'staging.todo.up.km.ua', user: 'deployer',  roles: %w{web app db}, port: 2201
set :branch, 'fixbug/TODO-43a-staging-deploy-fix'
set :rails_env, 'staging'
set :stage, 'staging'
set :sidekiq_env, 'staging'
set :deploy_to, '/home/deployer/apps/staging.todo.up.km.ua'
