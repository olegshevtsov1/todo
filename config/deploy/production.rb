server 'todo.up.km.ua', user: 'deployer', roles: %w{web app db}, port: 2201
set :branch, 'fixbug/TODO-43a-staging-deploy-fix'
set :rails_env, :production
set :stage , :production
set :sidekiq_env, :production
set :deploy_to, '/home/deployer/apps/todo.up.km.ua'