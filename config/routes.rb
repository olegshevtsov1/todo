# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'projects#index'
  resources :projects do
    resources :tasks do
    patch :done, on: :member
    end
    post :sort, on: :collection
  end
  namespace :api do
    resource :tinymce_images, only: :create
  end
end
