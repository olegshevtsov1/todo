# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do

  it_renders_404 :edit, :update, :destroy

  describe 'PUT #update' do
    let(:project) { create(:project) }
    context 'with valid attributes' do
      let(:params) { {id: project.to_param, project: {title: 'Project2'}} }
      it 'it update project should redirect projects_path should be valid ' do
        put :update, params: params
        expect(response).to redirect_to(projects_path)
      end
    end

    context 'with invalid attributes' do
      let(:params) { {id: project.to_param, project: {title: ''}} }
      it 'it update  project should render_template edit should be valid' do
        put :update, params: params
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:params) { {project: {title: 'Title1'}} }
      it 'create new project Project.count+1 should be valid' do
        post :create, params: params
        expect(Project.count).to eq(1)
      end
      it 'create new project redirect to projects_path should be valid' do
        post :create, params: params
        expect(response).to redirect_to(projects_path)
      end
    end

    context 'with invalid attributes' do
      let(:params) { {project: {title: ''}} }
      it 'does not create new project Project.count+1 should be not valid' do
        post :create, params: params
        expect(Project.count).to eq(0)
      end
      it 'does not create new project render_template new should be valid' do
        post :create, params: params
        expect(response).to render_template('new')
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:project) { create(:project) }
    context 'with valid attributes' do
      it 'destroy project Project.count-1 should be valid' do
        params = {id: project.to_param}
        expect do
          delete :destroy, params: params
        end.to change(Project, :count).by(-1)
      end

      context 'with valid attributes' do
        it 'destroy project redirect to projects_path should be valid' do
          delete :destroy, params: {id: project.to_param}
          expect(response).to redirect_to(projects_path)
        end
      end
    end
  end

  describe 'GET #index' do
    context 'renders index template' do
      it 'should be valid' do
        get :index
        expect(response).to render_template('index')
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET #new' do
    context 'renders new template' do
      it 'should be valid' do
        get :new
        expect(response).to render_template('new')
        expect(response).to be_successful
      end
    end
  end
end