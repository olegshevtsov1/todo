# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Project, type: :model do
  subject { create(:project) }
  describe 'should has fields' do
    it { expect(subject).to respond_to(:title) }
  end

  describe 'should has associations' do
    context 'has_many tasks' do
      subject { described_class.reflect_on_association(:tasks) }
      it { expect(subject.macro).to eq :has_many }
    end
  end

  describe 'when title is not present' do
    it 'should be not valid' do
      subject.title = ''
      expect(subject).to_not be_valid
    end
  end

  describe 'when title is  present' do
    it 'should be valid' do
      expect(subject).to be_valid
    end
  end
end
