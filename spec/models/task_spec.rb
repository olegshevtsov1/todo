# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Task, type: :model do
  subject { create(:task) }
  describe 'should be valid ' do
    it { expect(subject).to be_valid }
  end

  describe 'should has fields' do
    it { expect(subject).to respond_to(:title) }
    it { expect(subject).to respond_to(:deadline) }
    it { expect(subject).to respond_to(:project_id) }
    it { expect(subject).to respond_to(:status_id) }
    it { expect(subject).to respond_to(:priority_id) }
  end

  describe 'should has associations' do
    context 'belongs_to project'
    subject { described_class.reflect_on_association(:project) }
    it { expect(subject.macro).to eq :belongs_to }

    context 'belongs_to priority' do
      subject { described_class.reflect_on_association(:priority) }
      it { expect(subject.macro).to eq :belongs_to }
    end

    context 'belongs_to status' do
      subject { described_class.reflect_on_association(:status) }
      it { expect(subject.macro).to eq :belongs_to }
    end
  end

  describe 'when title is not present' do
    it 'should be not valid' do
      subject.title = ''
      expect(subject).to_not be_valid
    end
  end

  describe 'when title is  present' do
    context 'should be valid' do
      it { expect(subject).to be_valid }
    end
  end
end
