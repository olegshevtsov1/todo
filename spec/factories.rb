# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    title { 'Project1' }
  end

  factory :status do
    title { 'new' }
  end

  factory :priority do
    title { 'low' }
  end

  factory :task do
    title { 'Task1' }
    deadline { '04.09.2019' }
    project
    status
    priority
  end
end
