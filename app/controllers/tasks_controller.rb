# frozen_string_literal: true

class TasksController < ApplicationController
  before_action :task, only: %i[show edit update destroy done]

  def create
    task
    task.assign_attributes(task_params)
    if task.save
      redirect_to projects_path, notice: 'Task was successfully added.'
    else
      redirect_to projects_path, notice: 'Task was not added.'
    end
  end

  def edit
    task
  end

  def update
    if task.update(task_params)
      redirect_to projects_path, notice: 'Task was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    if task.destroy
      redirect_to projects_path, notice: 'Task was deleted successfully .'
    else
      redirect_to projects_path, alert: task.errors.full_messages
    end
  end

  def done
    if task.update(status_id: params[:task][:status])
      redirect_to projects_path, notice: 'Task was mark as done.' if params[:task][:status] == '4'
      redirect_to projects_path, notice: 'Task is back to in_progress.' if params[:task][:status] == '2'
    else
      render projects_path, notice: ['Errors:', task.errors.full_messages.join(',')].join
    end
  end

  private

  def task
    @task ||= params[:id].present? ? tasks.find(params[:id]) : tasks.new
  end

  def tasks
    @tasks ||= project.tasks
  end

  def project
    @project ||= begin
      Project.find(params[:project_id])
                 rescue StandardError
                   nil
    end
  end

  def task_params
    params.require(:task).permit(:project_id, :title, :description, :deadline, :priority_id, :status_id, :tinymce_image)
  end
end
