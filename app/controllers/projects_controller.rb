# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :project, only: %i[show edit update destroy], except: :sort
  protect_from_forgery except: [:sort]
  def index
    @projects = Project.all.order(:position)
    @task = Task.new
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    if @project.save
      redirect_to projects_path, notice: 'Project was successfully added.'
    else
      render :new, alert: @project.errors.full_messages
    end
  end

  def update
    if @project.update(project_params)
      redirect_to projects_path, notice: 'Project was successfully updated.'
    else
      render :edit, alert: @project.errors.full_messages
    end
  end

  def destroy
    if @project.destroy
      redirect_to projects_path, notice: 'Project was deleted successfully .'
    else
      redirect_to projects_path, alert: @project.errors.full_messages
    end
  end

  def sort
    Project.reorder(params[:project])
    head :no_content
  end

  private

  def project
    @project ||= begin
                   Project.find(params[:id])
                 rescue StandardError
                   nil
                 end
    render_404 unless @project
  end

  def project_params
    params.require(:project).permit(:title)
  end
end
