# frozen_string_literal: true

module Api
  class TinymceImagesController < ApplicationController
    def create
      blob = ActiveStorage::Blob.create_after_upload!(
        io: tinymce_image_params[:file],
        filename: tinymce_image_params[:file].original_filename,
        content_type: tinymce_image_params[:file].content_type
      )
      render json: {
        image: { url: url_for(blob) }
      }, content_type: 'text/html'
    end

    private

    def tinymce_image_params
      params.permit(:file)
    end
  end
end
