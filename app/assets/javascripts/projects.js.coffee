$ ->
  new Projects()

class Projects
  constructor: ->
    @init_sortable()

  init_sortable: =>
    $('#projects_sortable').sortable
      axis: 'y'
      handle: '.handle'
      update: ->
        $.post($(this).data('update-url'), $(this).sortable('serialize'))
