# frozen_string_literal: true

class Task < ApplicationRecord
  belongs_to :project
  belongs_to :priority
  belongs_to :status

  has_one_attached :tinymce_image

  validates :title, presence: true
  default_scope { order(:created_at) }
end
