# frozen_string_literal: true

class Priority < ApplicationRecord
  has_many :tasks
end
