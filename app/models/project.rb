# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :tasks, dependent: :destroy

  validates :title, presence: true

  acts_as_list

  def self.reorder(order_params)
    order_params.each_with_index do |id, index|
      Project.find(id).update!(position: index + 1)
    end
  end
end
