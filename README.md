# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:


* Ruby version <br>
    ruby 2.5.1

* Database creation <br>
    rake db:setup
    
* Demo App 
    http://staging.todo.up.km.ua:8080/
    
* Trello 
    https://trello.com/b/lhrToV98/todo    

## The Task
I'm person who passionate about mu own productivity. I want to manage my tasks and projects more effectively. I need a simple tool that supports me in controlling my task-flow.

Functional requirements

I wan to be able to create/update/delete projects
I want to be able to add tasks to my projects
I want to be able to update/delete tasks
I want to be able to prioritize tasks into a project
I want to be able to choose deadline for my task
I want to be able to mark a task as 'done'
Technical requirements
1. It should be a WEB application
2. For the client side must be used:
HTML, CSS (any libs as Twitter Bootstrap, Blueprint ...),
JavaScript (any libs as jQuery, Prototype ...)
3. For a server side any language as Rube, PHP, Python, JavaScript, C#, Java...
4. It Should have a client side and server side validation
5. It should look like on screens (see attach file 'rg_test_task_grid.png')

Additional requirements
- get all statuses, not repeating , alphabetically ordered
- get the of all tasks in each project, order by tasks count descending
- get the count of all tasks in each project, order by projects names
- get the list of projects without tasks and tasks with project_id = NULL
- get list of having several matches of both name and status, from the project 'Garage'. Order by matches count
- get the list of project names having more than 10 tasks in status 'completed'. Order by project_id
