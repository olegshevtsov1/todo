# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Status.create!([{ "title": "new"},
                { "title": "in_process"},
                { "title": "completed"},
                { "title": "done"}])

Priority.create!([{ "title": "low"},
                  { "title": "medium"},
                  { "title": "hight"}])


Project.create!([{ "title": "Complete the test task for Ruby Garage"},
                  { "title": "Project2"},
                  { "title": "Project3"}])

Task.create!([{ "title": "Task1", project_id: 1, status_id: 1},
                       { "title": "Task2", project_id: 1, status_id: 4},
                       { "title": "Task3", project_id: 2, status_id: 2},
                       { "title": "Task4", project_id: 2, status_id: 4},
                       { "title": "Task5", project_id: 3, status_id: 3},
                       { "title": "Task5_1", project_id: 3, status_id: 3},
                       { "title": "Task5_2", project_id: 3, status_id: 3},
                       { "title": "Task5_3", project_id: 3, status_id: 3},
                       { "title": "Task5_4", project_id: 3, status_id: 3},
                       { "title": "Task5_5", project_id: 3, status_id: 3},
                       { "title": "Task5_6", project_id: 3, status_id: 3},
                       { "title": "Task5_7", project_id: 3, status_id: 3},
                       { "title": "Task5_8", project_id: 3, status_id: 3},
                       { "title": "Task5_9", project_id: 3, status_id: 3},
                       { "title": "Task5_10", project_id: 3, status_id: 3},
                       { "title": "Task6", project_id: 3, status_id: 4}])
