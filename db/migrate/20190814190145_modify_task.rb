class ModifyTask < ActiveRecord::Migration[5.2]
  change_table :tasks do |t|
    t.change :deadline, :date, null: true
    t.change :title, :text, null: false
  end

  change_table :tasks do |t|
    t.belongs_to :priority, index: true, default: 2
    t.belongs_to :status, index: true, default: 1
  end
end
