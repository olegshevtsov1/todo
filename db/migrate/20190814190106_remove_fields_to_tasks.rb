class RemoveFieldsToTasks < ActiveRecord::Migration[5.2]
  def change
    remove_column :tasks, :priority_id
    remove_column :tasks, :status_id
  end
end
