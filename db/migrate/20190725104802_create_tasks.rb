class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.belongs_to :project, index: true
      t.string 	 :title, null: false
	  t.datetime :deadline, null: false
	  t.integer  :priority_id , null: false, default: 0
	  t.integer  :status_id , null: false, default: 0
      t.timestamps
    end
  end
end
